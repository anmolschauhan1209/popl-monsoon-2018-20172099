#lang eopl

(define-datatype bst-node bst-node?
  (null-node)
  (node (value number?)
        (left bst-node?)
        (right bst-node?)))

(define (create-node value)
  (node value (null-node) (null-node)))

(define (recreate-node value left-subtree right-subtree)
  (node value left-subtree right-subtree))

(define (get-subtree bst child)
  (cases bst-node bst
    (null-node () 0)
    (node (value left right)
      (cond
        [(equal? child "left") left]
        [(equal? child "right") right]))))


(define (get-value bst)
  (cases bst-node bst
    (null-node () 0)
    (node (value left right) value)))

(define (bst-insert bst value)
  (cond
    [(equal? bst (null-node)) (create-node value)]
    [(< value (get-value bst)) (recreate-node (get-value bst) (bst-insert (get-subtree bst "left") value) (get-subtree bst "right"))]
    [else (recreate-node (get-value bst) (get-subtree bst "left") (bst-insert (get-subtree bst "right") value))]))

(define (create-tree list1 bst index)
  (cond
    [(equal? index (length list1)) bst]
    [else (create-tree list1 (bst-insert bst (list-ref list1 index)) (+ index 1))]))

(define (build-tree list1)
  (create-tree list1 (null-node) 0))
  
(provide build-tree)